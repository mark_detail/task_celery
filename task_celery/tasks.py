from __future__ import absolute_import, unicode_literals
from task_celery.Celery import app
import time

@app.task
def add(x, y):
    return x + y

@app.task
def mul(x, y):
    return x * y

@app.task
def xsum(numbers):
    return sum(numbers)


@app.task
def add_longtime(a, b):
    print ('long time task begins')
    # sleep 5 seconds
    time.sleep(1)
    print ('long time task finished')
    return a + b


if __name__ == '__main__':
    add_longtime(1,2)


"""
celery  -A task_celery worker --loglevel=info
celery -A task_celery flower
python -m task_celery.run_tasks 



from app import Celery

# 中间件，这里使用RabbitMQ，pyamqp://Uername:Password@IP:Port//v_host
broker_url = 'pyamqp://development:guest@http://localhost:15672///development_host'

# 后端储存，这里使用RabbitMQ，rpc://Uername:Password@IP:Port//v_host
backend_url = 'rpc://development:guest@http://localhost:15672///development_host'

# 实例化一个celery对象
app = Celery('tasks', broker=broker_url, backend=backend_url)

# 设置配置参数的文件，创建文件celeryconfig.py来配置参数
app.config_from_object('celery_config')


 
 """