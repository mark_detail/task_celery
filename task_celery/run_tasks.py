from task_celery.tasks import add_longtime
import time

if __name__ == '__main__':
    name = add_longtime.name
    print('Task name  ', name)
    result = add_longtime.delay(1,2)
    #此时，任务还未完成，它将返回False
    print ('Task finished? ', result.ready())
    print ('Task result: ', result.result)
    # 延长到10秒以确保任务已经完成
    time.sleep(20)
    # 现在任务完成，ready方法将返回True
    print ('Task finished? ', result.ready())
    print ('Task result: ', result.result)
