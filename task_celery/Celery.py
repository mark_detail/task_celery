from __future__ import absolute_import
from celery import Celery

app = Celery('task_celery',
            broker='amqp://test:test123@localhost/test_vhost',
            backend='rpc://',
            include=['task_celery.tasks']
             )

# if __name__ == '__main__':
#     app.start()

